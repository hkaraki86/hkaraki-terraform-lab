variable "instance_type" {
 default = "t2.micro"
}

variable "instance_count" {
 default = "2"
}

variable "auth_key_name" {
 default = "authentication-key"
 }
  
variable "auth_public_key" {
 default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZjbmPLRYwxCT3Y70npFU08nietoQcC/kZ8PqaGioC2WI6DEWF39f6CQOTQEgz0Al+qWge5JFz+RG7pOsOQXC/2qTQeNwq8OfGzW4FCzxTFbqI+PTrs7+f174ILbd6oZYgRGrgYObr5rFSrA/7owKXJpOiRDnKUSWy7nrO6IDWdC88froec8TT+pnfTGbxGKqWKZWm7mXYKLnymz2ovJvJ6BtckMcBq5HGQt9uIGe6tL6chBCWmoVOvN1W/Sku/WlEYN9iEITpLik9iZYN2dcAZo07jYSyUMMtCztxEamVUoLa6CoxU9mbwXHcCBhW+61L+1reWvmKxNQhNYIWSuZL spark-user@baseImage"
}
