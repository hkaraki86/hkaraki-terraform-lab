provider "aws" {
  access_key = "AKIAUZXKVNYMT32QRZWI"
  secret_key  = "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"
  region          = "us-east-1"
}

resource "aws_instance" "Atlantis" {
  instance_type = "${var.instance_type}"
  ami = "ami-03597b1b84c02cf7b"
  count = 2
  key_name = "${var.auth_key_name}"
  vpc_security_group_ids = ["${aws_security_group.VPC_Gateway.id}"]
  associate_public_ip_address = true
}

resource "aws_key_pair" "push_auth_key" {
  key_name   = "${var.auth_key_name}"
  public_key = "${var.auth_public_key}"
  }

resource "aws_security_group" "VPC_Gateway" {
  ingress {
		from_port = 0
		to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

resource "aws_elb" "load_balancer" {

 name = "my-elb"
 availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1e"]
 instances = ["${aws_instance.Atlantis.*.id}"]
 cross_zone_load_balancing = true
 idle_timeout = 400
 connection_draining = true
 connection_draining_timeout = 400
 
  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  
    health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8080/"
    interval            = 30
  }
  
}

